<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>MGM - Game Play Page</title>
		
		<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
		<script src="resources/js/phaser-2-10-3.js"></script>
		
		<script type="text/javascript">
			var mapData = ${scene.nodecontent};
		</script>
		<script type="text/javascript" src="resources/js/util.js"></script>
		<script type="text/javascript" src="resources/js/playGame.js"></script>
	</head>
	<body>
	</body>
</html>