<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>MGM - Script Add</title>
		
		<link rel="stylesheet" href="resources/css/scriptEditor.css">
	</head>
	<body>
		이벤트 추가~
		<div class="dialog">
			<div class="dialog-container image-left">
				<img class="actor-photo" src="resources/img/not_profile.jpg" style="height: 100px; width: 100px">
				<p class="dialog-actor-name"> NPC 기본 </p>
				<p class="dialog-text"> 대화명 입력 </p>
			</div>
		</div>
		
		<script type="module" src="resources/js/scriptEdit.js"></script>
	</body>
</html>